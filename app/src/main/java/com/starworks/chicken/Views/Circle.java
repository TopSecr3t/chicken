package com.starworks.chicken.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.starworks.chicken.R;

/**
 * Created by egral on 09/03/15.
 */
public class Circle extends View {

    private Paint backgroundPaint;

    RectF outerBounds;

    public Circle(Context context, AttributeSet attrs){
        super(context, attrs);
        init();

    }

    private void init(){
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(0xffdddddd);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        outerBounds = new RectF(getPaddingLeft(), getPaddingTop(), w-getPaddingRight(), h-getPaddingBottom());

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawOval(outerBounds, backgroundPaint);
    }
}
