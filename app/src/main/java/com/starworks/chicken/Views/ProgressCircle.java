package com.starworks.chicken.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.starworks.chicken.R;

/**
 * Created by egral on 09/03/15.
 */
public class ProgressCircle extends View {
    private double progress; // between 0 and 1

    private Paint progressPaint;
    private Paint backgroundPaint;
    private Paint shadowPaint;

    float thickness;

    RectF outerBounds;
    RectF innerBounds;

    public ProgressCircle(Context context, AttributeSet attrs){
        super(context, attrs);
        init();

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ProgressCircle,
                0, 0);

        try {
            thickness = a.getDimension(R.styleable.ProgressCircle_bar_thickness, 5f);
        } finally {
            a.recycle();
        }

    }

    private void init(){
        progressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressPaint.setColor(0xaa337799);

        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(0xffeeeeee);

        shadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        shadowPaint.setColor(0xffcccccc);
    }

    public double getProgress(){
        return progress;
    }

    public void setProgress(double prog){
        progress = prog;

        progressPaint.setColor(Color.rgb((int)(progress*255), 0x77, 0x99));

        invalidate();
        requestLayout();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        float t = thickness;

        outerBounds = new RectF(getPaddingLeft(), getPaddingTop(), w-getPaddingRight(), h-getPaddingBottom());
        innerBounds = new RectF(outerBounds.left+t, outerBounds.top+t, outerBounds.right-t, outerBounds.bottom-t);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawOval(outerBounds, shadowPaint);
        canvas.drawArc(outerBounds,-90f,-(float)(1-progress)*360,true, progressPaint);
        canvas.drawOval(innerBounds, backgroundPaint);
    }
}
