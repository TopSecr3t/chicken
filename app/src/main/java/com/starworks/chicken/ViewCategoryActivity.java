package com.starworks.chicken;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.starworks.chicken.ArrayAdapters.CategoryChallengesListAdapter;
import com.starworks.chicken.Models.Category;


public class ViewCategoryActivity extends ActionBarActivity {

    Category category;

    ListView lstChallenges;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_category);

        Intent intent = getIntent();
        int catID = intent.getIntExtra(MainActivity.CATEGORY_ID, -1);
        category = DataStore.getCategory(catID);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(category.getName());

        lstChallenges = (ListView) findViewById(R.id.lstChallenges);
        lstChallenges.setAdapter(new CategoryChallengesListAdapter(this, category.getRemainingChallenges()));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        switch (id){
//           //noinspection SimplifiableIfStatement
//            case R.id.action_settings:
//                return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
