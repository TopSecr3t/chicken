package com.starworks.chicken;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.starworks.chicken.Fragments.CategoriesFragment;
import com.starworks.chicken.Fragments.HomeFragment;
import com.starworks.chicken.Fragments.NavigationDrawerFragment;
import com.starworks.chicken.Models.User;


public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String CATEGORY_ID = "category_id";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;

    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    /* A flag indicating that a PendingIntent is in progress and prevents
     * us from starting further intents.
     */
    private boolean mIntentInProgress;

    private static int failureCount = 0;

    private Button gpButton;
    private LoginButton fbButton;
    private TextView txtUserPoints;

    private CallbackManager mCallbackManager;
    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            fbConnected(profile);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_main);

        //Inicializacija ImageLoaderja
        ImageHelper.init(this);

        // Nastavi context vsem razredom, ki ga potrebujejo
        OnlineDB.setContext(this);
        DBHandler.setContext(this);
        FirstLaunch.setContext(this);
        PointsHandler.setContext(this);
        User.setContext(this);
        // Če je aplikacija prvič zagnana, nastavi privzete vrednosti
        FirstLaunch.check();
        // Nadgradi lokalno bazo, da je enaka kot na spletu
        OnlineDB.refreshDB();
        DataStore.refresh();

        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        fbButton = (LoginButton)findViewById(R.id.login_button);
        fbButton.registerCallback(mCallbackManager, mCallback);
        gpButton = (Button)findViewById(R.id.gpButton);
        txtUserPoints = (TextView)findViewById(R.id.txtUserPoints);
        if (User.isSignedIn()) {
            fbButton.setVisibility(View.GONE);
            gpButton.setVisibility(View.GONE);
        } else {
            gpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gpOnClick();
                }
            });
        }

        PointsHandler.refreshPoints();



        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment;

        switch(position){
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                final ListView lv = ((ListView)mNavigationDrawerFragment.getView().findViewById(R.id.lstDrawerMenu));
                fragment = new CategoriesFragment();
                break;
            default:
                fragment = new HomeFragment();
                break;
        }

        Bundle args = new Bundle();
        args.putInt(Constants.ARG_SECTION_NUMBER, position);
        fragment.setArguments(args);

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    public void onSectionAttached(int number) {
        mTitle = getResources().getStringArray(R.array.drawer_menu)[number];
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavigationDrawerFragment.updateUserData();
    }

    public void onConnectionFailed(ConnectionResult result) {
        if (!mIntentInProgress && result.hasResolution()) {
            try {
                mIntentInProgress = true;
                startIntentSenderForResult(result.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                if (failureCount < 5) {
                    mGoogleApiClient.connect();
                    failureCount++;
                }
                else {
                    finish();
                    Toast.makeText(this, "Failed to connect!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void onConnected(Bundle connectionHint) {
        mSignInClicked = false;
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person user = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

            if (!User.isSignedIn()) {
                SharedPreferences sharedPref = getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("id", user.getId());
                editor.putString("name", user.getDisplayName());
                editor.putString("image", user.getImage().getUrl() + "&sz=100");
                editor.apply();
            }

            txtUserPoints.setText("0p");

            fbButton.setVisibility(View.GONE);
            gpButton.setVisibility(View.GONE);

            Toast.makeText(this, "Successfully connected to Google+!", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }

        mCallbackManager.onActivityResult(requestCode, responseCode, intent);
    }

    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    /* Track whether the sign-in button has been clicked so that we know to resolve
 * all issues preventing sign-in without waiting.
 */
    private boolean mSignInClicked;

    /* Store the connection result from onConnectionFailed callbacks so that we can
     * resolve them when the user clicks sign-in.
     */
    private ConnectionResult mConnectionResult;

    /* A helper method to resolve the current ConnectionResult error. */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void gpOnClick() {
        if (isNetworkAvailable()) {
            if (!User.isSignedIn()) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(Plus.API)
                        .addScope(Plus.SCOPE_PLUS_PROFILE)
                        .build();
                mGoogleApiClient.connect();
            }
        } else
            Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT).show();
    }

    private void fbConnected(Profile profile) {
        fbButton.setVisibility(View.GONE);
        gpButton.setVisibility(View.GONE);

        if (!User.isSignedIn()) {
            SharedPreferences sharedPref = getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("id", profile.getId());
            editor.putString("name", profile.getName());
            editor.putString("image", profile.getProfilePictureUri(100, 100).toString());
            editor.apply();
        }

        txtUserPoints.setText("0p");

        fbButton.setVisibility(View.GONE);
        gpButton.setVisibility(View.GONE);

        Toast.makeText(this, "Successfully connected to Facebook!", Toast.LENGTH_SHORT).show();
    }
}
