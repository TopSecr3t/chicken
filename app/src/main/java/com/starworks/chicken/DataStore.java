package com.starworks.chicken;

import com.starworks.chicken.Models.ActiveChallenge;
import com.starworks.chicken.Models.Category;
import com.starworks.chicken.Models.Challenge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lojze on 20.2.2015.
 */

//Zacasna shramba podatkov o izzivih in kategorijah
public class DataStore {
    public static List<Category> categories;
    public static List<Challenge> challenges;
    public static List<ActiveChallenge> activeChallenges;

    public static void refresh(){
        categories = Category.getAllCategories();

        if(challenges == null)
            challenges = Challenge.getAllChallenges();
        else
            challenges = Challenge.getRemainingChallenges();

        activeChallenges = ActiveChallenge.getActiveChallenges();
    }

    public static List<Category> getRandomCategoriesList(int size){
        List<Category> randCat = new ArrayList<Category>();
        for(int i=0; i<size; i++){
            randCat.add(categories.get((int)(Math.random()*categories.size())));
        }

        return randCat;
    }

    public static Category getCategory(int id){
        for(int i=0; i<categories.size(); i++){
            if(categories.get(i).getID()==id){
                return categories.get(i);
            }
        }
        return null;
    }

    public static Challenge getChallenge(int i){
        for(int j=0; j<challenges.size(); j++){
            if(challenges.get(j).getID()==i)
                return challenges.get(j);
        }

        return null;
    }
}

