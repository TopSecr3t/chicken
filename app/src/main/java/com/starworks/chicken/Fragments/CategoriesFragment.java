package com.starworks.chicken.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.starworks.chicken.ArrayAdapters.CategoriesGridAdapter;
import com.starworks.chicken.DataStore;
import com.starworks.chicken.Listeners.CategoryOnClickListener;
import com.starworks.chicken.MainActivity;
import com.starworks.chicken.R;

public class CategoriesFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    GridView grdCategories;
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public void setSectionNumber(int sectionNumber) {
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        setArguments(args);
    }

    public CategoriesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_categories, container, false);

        grdCategories = (GridView) rootView.findViewById(R.id.grdCategories);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        grdCategories.setAdapter(new CategoriesGridAdapter(getActivity(), DataStore.categories));

        grdCategories.setOnItemClickListener(new CategoryOnClickListener(getActivity()));
    }
}
