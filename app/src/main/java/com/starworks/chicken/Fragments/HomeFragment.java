package com.starworks.chicken.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.starworks.chicken.ArrayAdapters.ActiveChallengeListAdapter;
import com.starworks.chicken.ArrayAdapters.CategoriesGridAdapter;
import com.starworks.chicken.ArrayAdapters.SuggestedChallengeListAdapter;
import com.starworks.chicken.Constants;
import com.starworks.chicken.DataStore;
import com.starworks.chicken.Listeners.CategoryOnClickListener;
import com.starworks.chicken.Listeners.ChallengeOnClickListener;
import com.starworks.chicken.MainActivity;
import com.starworks.chicken.Models.ActiveChallenge;
import com.starworks.chicken.R;

import java.util.List;

public class HomeFragment extends Fragment {
    public static HomeFragment fragment;

    LinearLayout lstActiveChallenges;
    LinearLayout lstSuggestedChallenges;
    GridView grdCategories;

    TextView btnCategoriesMore;

    ActiveChallengeListAdapter activeChallengesAdapter;
    SuggestedChallengeListAdapter suggestedChallengesAdapter;

    public HomeFragment(){
        fragment = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        rootView.findViewById(R.id.grdCategories).setFocusable(false);

        activeChallengesAdapter = new ActiveChallengeListAdapter(inflater);
        lstActiveChallenges = (LinearLayout)rootView.findViewById(R.id.lstActiveChallenges);
        activeChallengesAdapter.populateLayout(lstActiveChallenges);

        suggestedChallengesAdapter = new SuggestedChallengeListAdapter(inflater);
        lstSuggestedChallenges = (LinearLayout)rootView.findViewById(R.id.lstSuggestedChallenges);
        suggestedChallengesAdapter.populateLayout(lstSuggestedChallenges);

        grdCategories = (GridView) rootView.findViewById(R.id.grdCategories);

        btnCategoriesMore = (TextView)rootView.findViewById(R.id.btnCategoryMore);
        btnCategoriesMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).onNavigationDrawerItemSelected(1);
            }
        });

        rootView.findViewById(R.id.scrollView).setOnTouchListener(new View.OnTouchListener() {
            boolean closed = false;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(!closed && event.getAction()==MotionEvent.ACTION_MOVE ){
                    activeChallengesAdapter.closeAll();
                    suggestedChallengesAdapter.closeAll();
                    closed = true;
                }
                if (event.getAction() == MotionEvent.ACTION_UP){
                    closed = false;
                }
                return false;
            }
        });


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(Constants.ARG_SECTION_NUMBER));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        grdCategories.setAdapter(new CategoriesGridAdapter(getActivity(), DataStore.categories).setLimit(1));
        grdCategories.setOnItemClickListener(new CategoryOnClickListener(getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();

        List<ActiveChallenge> challenges = DataStore.activeChallenges;
        for(int i=0; i<challenges.size(); i++){
            if(challenges.get(i).getProgress()>1){
                createAndShowAlertDialog(i);
            }
        }
    }

    public void updateLists(){
        //TODO: Posodobitev seznamov ob spremembah
        DataStore.refresh();
        activeChallengesAdapter.populateLayout(lstActiveChallenges);
        suggestedChallengesAdapter.populateLayout(lstSuggestedChallenges);
    }

    private void createAndShowAlertDialog(int id) {
        final ActiveChallenge ch = DataStore.activeChallenges.get(id);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(ch.getName());
        builder.setMessage(String.format("The time for the challenge %s is up! Have you completed the challenge?",ch.getName()));
        builder.setPositiveButton("Completed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ch.setCompleted();
                updateLists();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Failed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ch.setFailed();
                updateLists();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
