package com.starworks.chicken;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.starworks.chicken.Models.User;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Aljaz on 10/03/15.
 */
public class PointsHandler {
    private static final String GET_POINTS_PHP = "http://ayangames.com/chicken/get_points.php";
    private static final String ADD_USER_PHP = "http://ayangames.com/chicken/add_user.php";
    private static final String UPDATE_POINTS_PHP = "http://ayangames.com/chicken/update_points.php";

    private static Context cont;

    private static String userId;
    private static String userName;

    public static void setContext(Context context) {
        cont = context;
    }

    public static void refreshPoints() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);
                if (sharedPref.getString("id", "0").equals("0")) {
                    return;
                }
                userId = sharedPref.getString("id", "0");
                userName = sharedPref.getString("name", "0");
                userName = userName.replaceAll(" ","");
                InputStream inputStream = null;
                String JSON = "";
                int points = 0;

                // vspostavi povezavo z php skripto
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(GET_POINTS_PHP + "?id=" + userId);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    inputStream = httpEntity.getContent();
                } catch (Exception e) {
                    Log.e("log_tag", "Napaka v http povezavi: " + e.toString());
                    return;
                }
                // shrani bazo v jSon obliki
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
                    StringBuilder builder = new StringBuilder();
                    String vrsta = reader.readLine();

                    while (vrsta != null) {
                        builder.append(vrsta + "\n");
                        vrsta = reader.readLine();
                    }
                    inputStream.close();
                    JSON = builder.toString();
                } catch (Exception e) {
                    Log.e("log_tag", "Napaka pri prevajanju rezultata: " + e.toString());
                    return;
                }
                try {
                    JSONArray jArray = new JSONArray(JSON);

                    JSONObject jObject = jArray.getJSONObject(0);
                    points = Integer.parseInt(jObject.getString("Points"));
                } catch (Exception e) {
                    addUser();
                    Log.e("log_tag", "Napaka pri vpisu podatkov v String: " + e.toString());
                    return;
                }

                if (points > User.getPoints()) {
                    User.setPoints(points);
                } else {
                    updatePoints();
                }
            }
        };
        Thread thread = new Thread(r);
        thread.start();
    }

    private static void addUser() {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(ADD_USER_PHP + "?id=" + userId + "&points=" + User.getPoints() + "&name=" + userName);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream = httpEntity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Napaka v http povezavi: " + e.toString());
            return;
        }
    }

    private static void updatePoints() {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(UPDATE_POINTS_PHP + "?id=" + userId + "&points=" + User.getPoints());
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream inputStream = httpEntity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Napaka v http povezavi: " + e.toString());
            return;
        }
    }
}
