package com.starworks.chicken;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.starworks.chicken.Models.ActiveChallenge;
import com.starworks.chicken.Models.Category;
import com.starworks.chicken.Models.Challenge;
import com.starworks.chicken.Models.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aljaz on 21.2.2015.
 */
public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "chicken.db";
    private static final String TABLE_CHALLENGES = "challenges";

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NAME = "_name";
    private static final String COLUMN_DESCRIPTION = "_description";
    private static final String COLUMN_POINTS = "_points";
    private static final String COLUMN_DURATION = "_duration";
    private static final String COLUMN_CATEGORY = "_category";
    private static final String COLUMN_GROUP = "_group";
    private static final String COLUMN_GROUP_ORDER = "_group_order";

    private static final String TABLE_CATEGORIES = "categories";
    private static final String CATEG_COLUMN_ID = "_id";
    private static final String CATEG_COLUMN_NAME = "_name";
    private static final String CATEG_COLUMN_PICTUREURI = "_picture_uri";


    private static final String TABLE_STATUS = "status";
    private static final String STA_COLUMN_ID = "_id";
    private static final String STA_COLUMN_STATUS = "_status";  //1...accepted, 2...dismissed, 3...postponed, 4... completed, 5...failed
    private static final String STA_COLUMN_TIME = "_time";


    private static Context cont;

    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(cont, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_CHALLENGES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_POINTS + " INTEGER, " +
                COLUMN_DURATION + " INTEGER, " +
                COLUMN_CATEGORY + " INTEGER, " +
                COLUMN_GROUP + " INTEGER, " +
                COLUMN_GROUP_ORDER + " INTEGER" +
                ");";
        db.execSQL(query);

        String categQuery = "CREATE TABLE " + TABLE_CATEGORIES + "(" +
                CATEG_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                CATEG_COLUMN_NAME + " TEXT, " +
                CATEG_COLUMN_PICTUREURI + " TEXT " +
                ");";
        db.execSQL(categQuery);

        String statusQuery = "CREATE TABLE " + TABLE_STATUS + "(" +
                STA_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                STA_COLUMN_STATUS + " INTEGER, " +
                STA_COLUMN_TIME + " INTEGER " +
                ");";
        db.execSQL(statusQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHALLENGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS);
        onCreate(db);
    }

    public void updateDB(Challenge[] challenges) {
        SQLiteDatabase db = getWritableDatabase();

        for (int i = 1; i < challenges.length + 1; i++) {
            db.execSQL("DELETE FROM " + TABLE_CHALLENGES + "\nWHERE " + COLUMN_ID + "=" + i);
        }

        for (int i = 0; i < challenges.length; i++) {
            db.execSQL("INSERT INTO " + TABLE_CHALLENGES + "(" +
                            COLUMN_ID + ", " +
                            COLUMN_NAME + ", " +
                            COLUMN_DESCRIPTION + ", " +
                            COLUMN_POINTS + ", " +
                            COLUMN_DURATION + ", " +
                            COLUMN_CATEGORY + ", " +
                            COLUMN_GROUP + ", " +
                            COLUMN_GROUP_ORDER + ")\n" +
                            "VALUES (" +
                            "'" + challenges[i].getID() + "', " +
                            "'" + challenges[i].getName() + "', " +
                            "'" + challenges[i].getDescription() + "', " +
                            "'" + challenges[i].getPoints() + "', " +
                            "'" + challenges[i].getDuration() + "', " +
                            "'" + challenges[i].getCategory().getID() + "', " +
                            "'" + challenges[i].getGroup() + "', " +
                            "'" + challenges[i].getGroupOrder() + "');"
            );
        }
        db.close();
        updateStatus(challenges.length);
    }

    private void updateStatus(int length) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int staLength = 0;

        if (cursor.moveToFirst()) {
            do {
                staLength = Integer.parseInt(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        for (int n = 0; n < length - staLength; n++) {
            db.execSQL("INSERT INTO " + TABLE_STATUS + "('" +
                            STA_COLUMN_STATUS + "', '" +
                            STA_COLUMN_TIME + "')\n" +
                            "VALUES (" +
                            "'0', " +
                            "'');"
            );
        }
    }

    public void changeStatus(int id, int status) {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("UPDATE " + TABLE_STATUS +
                " SET " + STA_COLUMN_STATUS + "=" + status +
                " WHERE " + STA_COLUMN_ID + "=" + id + ";");

        db.execSQL("UPDATE " + TABLE_STATUS +
                " SET " + STA_COLUMN_TIME + "=" + System.currentTimeMillis() +
                " WHERE " + STA_COLUMN_ID + "=" + id + ";");

        DataStore.refresh();
//        SQLiteDatabase db2 = getReadableDatabase();
//        Cursor cursor = db.query(TABLE_STATUS, new String[] { STA_COLUMN_STATUS }, STA_COLUMN_ID + "=?",
//                new String[] { String.valueOf(id) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
    }

    public void updateDB(Category[] categories) {
        SQLiteDatabase db = getWritableDatabase();

        for (int i = 1; i < categories.length + 1; i++) {
            db.execSQL("DELETE FROM " + TABLE_CATEGORIES + "\nWHERE " + CATEG_COLUMN_ID + "=" + i);
        }

        for (int i = 0; i < categories.length; i++) {
            db.execSQL("INSERT INTO " + TABLE_CATEGORIES + "(" +
                            CATEG_COLUMN_ID + ", " +
                            CATEG_COLUMN_NAME + ", " +
                            CATEG_COLUMN_PICTUREURI + ")\n" +
                            "VALUES (" +
                            "'" + categories[i].getID() + "', " +
                            "'" + categories[i].getName() + "', " +
                            "'" + categories[i].getPictureRes() + "');"
            );
        }
        db.close();
    }

    public static void setContext(Context context) {
        cont = context;
    }

    public String getName(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHALLENGES, new String[] { COLUMN_NAME }, COLUMN_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getDescription(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHALLENGES, new String[] { COLUMN_DESCRIPTION }, COLUMN_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getString(0);
    }

    public int getPoints(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHALLENGES, new String[] { COLUMN_POINTS }, COLUMN_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return Integer.parseInt(cursor.getString(0));
    }

    public int getDuration(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHALLENGES, new String[] { COLUMN_DURATION }, COLUMN_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return Integer.parseInt(cursor.getString(0));
    }

    public Category getCategory(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHALLENGES, new String[] { COLUMN_CATEGORY }, COLUMN_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return new Category(Integer.parseInt(cursor.getString(0)));
    }

    public int getGroup(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHALLENGES, new String[] { COLUMN_GROUP }, COLUMN_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return Integer.parseInt(cursor.getString(0));
    }

    public int getGroupOrder(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHALLENGES, new String[] { COLUMN_GROUP_ORDER }, COLUMN_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return Integer.parseInt(cursor.getString(0));
    }


    public String getCategName(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CATEGORIES, new String[]{CATEG_COLUMN_NAME}, CATEG_COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getCategPictureURI(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_CATEGORIES, new String[]{CATEG_COLUMN_PICTUREURI}, CATEG_COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getString(0);
    }

    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<Category>();
        String query = "SELECT * FROM " + TABLE_CATEGORIES + " WHERE 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                categories.add(new Category(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2)

                ));
            } while (cursor.moveToNext());
        }
        return categories;
    }

    public List<Challenge> getChallenges(Category category) {
        List<Challenge> challenges = new ArrayList<Challenge>();
        String query;
        if (category == null)
            query = "SELECT * FROM " + TABLE_CHALLENGES + " WHERE 1";
        else
            query = "SELECT * FROM " + TABLE_CHALLENGES + " WHERE " + COLUMN_CATEGORY + "=" + category.getID();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                challenges.add(new Challenge(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),
                        Integer.parseInt(cursor.getString(3)),
                        Integer.parseInt(cursor.getString(4)),
                        new Category(Integer.parseInt(cursor.getString(5))),
                        Integer.parseInt(cursor.getString(6)),
                        Integer.parseInt(cursor.getString(7))
                ));
            } while (cursor.moveToNext());
        }
        return challenges;
    }

    public List<Challenge> getRemainingChallenges(Category category) {
        List<Challenge> challenges = new ArrayList<Challenge>();
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + STA_COLUMN_STATUS + "='0'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                if (category == null ||
                        (category != null && new Challenge(Integer.parseInt(cursor.getString(0))).getCategory().getID() == category.getID())) {
                    //if (getChallenges(category).get(Integer.parseInt(cursor.getString(0)) - 1).getGroup() == 0)
                        challenges.add(DataStore.getChallenge(Integer.parseInt(cursor.getString(0))));
                }
            } while (cursor.moveToNext());
        }
        return challenges;
    }

    public List<Group> getRemainingGroups(Category category) {
        List<Group> groups = new ArrayList<Group>();
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + STA_COLUMN_STATUS + "='0'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                if (category == null ||
                        (category != null && new Challenge(Integer.parseInt(cursor.getString(0))).getCategory().equals(category))) {
                    if (getChallenges(category).get(Integer.parseInt(cursor.getString(0)) - 1).getGroup() != 0 && getChallenges(category).get(Integer.parseInt(cursor.getString(0)) - 1).getGroupOrder() == 0)
                        groups.add(new Group(getChallenges(category).get(Integer.parseInt(cursor.getString(0)) - 1).getID()));
                }
            } while (cursor.moveToNext());
        }
        return groups;
    }

    public List<Challenge> getGroupChallenges(Group group) {
        List<Challenge> tempChallenges = getChallenges(null);
        List<Challenge> result = getChallenges(null);
        for (int n = 0; n < tempChallenges.size(); n++) {
            if (tempChallenges.get(n).getGroup() == group.getId() && tempChallenges.get(n).getGroupOrder() == 0)
                result.add(tempChallenges.get(n).getGroupOrder() - 1, tempChallenges.get(n));
        }
        return result;
    }

    public List<ActiveChallenge> getActiveChallenges(Category category) {
        List<ActiveChallenge> challenges = new ArrayList<ActiveChallenge>();
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + STA_COLUMN_STATUS + "='1'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                if (category == null ||
                        (category != null && new Challenge(Integer.parseInt(cursor.getString(0))).getCategory().equals(category))) {
                    Challenge tempCha = getChallenges(category).get(Integer.parseInt(cursor.getString(0)) - 1);
                    challenges.add(new ActiveChallenge(tempCha, Long.parseLong(cursor.getString(2))));
                }
            } while (cursor.moveToNext());
        }
        return challenges;
    }
}
