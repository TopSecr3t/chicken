package com.starworks.chicken.ArrayAdapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.starworks.chicken.Constants;
import com.starworks.chicken.Listeners.ChallengeOnClickListener;
import com.starworks.chicken.Listeners.SuggestedChallengeOptionOnClickListener;
import com.starworks.chicken.Models.Challenge;
import com.starworks.chicken.R;

import java.util.List;

/**
 * Created by Lojze on 25.2.2015.
 */
public class CategoryChallengesListAdapter extends ArrayAdapter<Challenge> {
    private static final int LAYOUT = R.layout.suggested_challenge_item;

    List<Challenge> challenges;
    Context context;



    public CategoryChallengesListAdapter(Context context, List<Challenge> challenges){
        super(context, LAYOUT, challenges);
        this.context = context;
        this.challenges = challenges;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if(view == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            view = inflater.inflate(LAYOUT, parent, false);
        }

        Challenge challenge = challenges.get(position);

        TextView lblName;
        TextView lblDescription;
        TextView lblDuration;

        lblName = (TextView) view.findViewById(R.id.lblChallengeName);
        lblDescription = (TextView) view.findViewById(R.id.lblChallengeDescription);
        lblDuration = (TextView) view.findViewById(R.id.lblDuration);

        lblName.setText(challenge.getName());
        lblDescription.setText(challenge.getDescription());
        lblDuration.setText(challenge.getDurationString());

        ChallengeOnClickListener challengeOnClickListener = new ChallengeOnClickListener();
        view.setOnClickListener(challengeOnClickListener);
        view.findViewById(R.id.viewChallengeOverlay).setOnClickListener(challengeOnClickListener);

        ViewGroup optionBar = (ViewGroup)view.findViewById(R.id.viewOptionsBar);
        for (int k = 0; k < optionBar.getChildCount(); k++){
            optionBar.getChildAt(k).setOnClickListener(new SuggestedChallengeOptionOnClickListener(challenge, view));
        }


        return view;
    }

}
