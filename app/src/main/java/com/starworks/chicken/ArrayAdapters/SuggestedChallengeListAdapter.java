package com.starworks.chicken.ArrayAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.starworks.chicken.DataStore;
import com.starworks.chicken.Listeners.ChallengeOnClickListener;
import com.starworks.chicken.Listeners.SuggestedChallengeOptionOnClickListener;
import com.starworks.chicken.Models.ActiveChallenge;
import com.starworks.chicken.Models.Challenge;
import com.starworks.chicken.Constants;
import com.starworks.chicken.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lojze on 25.2.2015.
 */
public class SuggestedChallengeListAdapter {
    private static final int LAYOUT = R.layout.suggested_challenge_item;
    private List<View> items;
    private LayoutInflater inflater;

    public SuggestedChallengeListAdapter(LayoutInflater inflater){
        this.inflater = inflater;
        items = new ArrayList<>();
    }

    public void populateLayout(LinearLayout layout) {
        layout.removeAllViewsInLayout();
        items.clear();

        List<Challenge> challenges = DataStore.challenges;
        for(int i=0; i< Constants.SUGGESTED_CHALLENGES_COUNT; i++) {
            int position = (int)(Math.random()*challenges.size());
            View item;

            TextView lblName, lblDescription, lblDuration;

            Challenge challenge = challenges.get(position);


            item = inflater.inflate(LAYOUT, null);

            lblName = (TextView) item.findViewById(R.id.lblChallengeName);
            lblDescription = (TextView) item.findViewById(R.id.lblChallengeDescription);
            lblDuration = (TextView) item.findViewById(R.id.lblDuration);

            lblName.setText(challenge.getName());
            lblDescription.setText(challenge.getDescription());
            lblDuration.setText(challenge.getDurationString());

            ChallengeOnClickListener challengeOnClickListener = new ChallengeOnClickListener();
            item.setOnClickListener(challengeOnClickListener);
            item.findViewById(R.id.viewChallengeOverlay).setOnClickListener(challengeOnClickListener);

            ViewGroup optionBar = (ViewGroup)item.findViewById(R.id.viewOptionsBar);
            for (int k = 0; k < optionBar.getChildCount(); k++){
                optionBar.getChildAt(k).setOnClickListener(new SuggestedChallengeOptionOnClickListener(challenge, item));
            }

            layout.addView(item);
            items.add(item);
        }
    }

    public void closeAll(){
        for(int i = 0; i<items.size(); i++){
            if(items.get(i).findViewById(R.id.viewChallengeOverlay).getVisibility()==View.GONE){
                items.get(i).callOnClick();
            }
        }
    }
}
