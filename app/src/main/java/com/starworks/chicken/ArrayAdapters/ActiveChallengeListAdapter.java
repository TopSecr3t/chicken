package com.starworks.chicken.ArrayAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.starworks.chicken.DataStore;
import com.starworks.chicken.Listeners.ActiveChallengeOptionOnClickListener;
import com.starworks.chicken.Listeners.ChallengeOnClickListener;
import com.starworks.chicken.Models.ActiveChallenge;
import com.starworks.chicken.R;
import com.starworks.chicken.Views.ProgressCircle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lojze on 25.2.2015.
 */
public class ActiveChallengeListAdapter {
    private static final int LAYOUT = R.layout.active_challenge_item;
    private List<View> items;
    private LayoutInflater inflater;

    public ActiveChallengeListAdapter(LayoutInflater inflater){
        this.inflater = inflater;
        items = new ArrayList<>();
    }

    public void populateLayout(LinearLayout layout) {
        layout.removeAllViewsInLayout();
        items.clear();

        List<ActiveChallenge> activeChallenges = DataStore.activeChallenges;
        for(int i=0; i<activeChallenges.size(); i++) {
            View item;

            ProgressCircle prgChallenge;
            TextView lblName, lblDescription, lblProgress;

            ActiveChallenge challenge = activeChallenges.get(i);
            LinearLayout viewChallengeInfo;

            item = inflater.inflate(LAYOUT, null);

            prgChallenge = (ProgressCircle) item.findViewById(R.id.prgChallenge);
            lblName = (TextView) item.findViewById(R.id.lblChallengeName);
            lblDescription = (TextView) item.findViewById(R.id.lblChallengeDescription);
            lblProgress = (TextView) item.findViewById(R.id.lblProgress);
            viewChallengeInfo = (LinearLayout) item.findViewById(R.id.viewChallengeOverlay);

            prgChallenge.setProgress(challenge.getProgress());
            lblName.setText(challenge.getName());
            lblDescription.setText(challenge.getDescription());
            lblProgress.setText(challenge.getTimeLeft());

            ChallengeOnClickListener challengeOnClickListener = new ChallengeOnClickListener();
            item.setOnClickListener(challengeOnClickListener);
            item.findViewById(R.id.viewChallengeOverlay).setOnClickListener(challengeOnClickListener);

            ViewGroup optionBar = (ViewGroup)item.findViewById(R.id.viewOptionsBar);
            for (int k = 0; k < optionBar.getChildCount(); k++){
                optionBar.getChildAt(k).setOnClickListener(new ActiveChallengeOptionOnClickListener(challenge, item));
            }

            layout.addView(item);
            items.add(item);
        }
    }

    public void closeAll(){
        for(int i = 0; i<items.size(); i++){
            if(items.get(i).findViewById(R.id.viewChallengeOverlay).getVisibility()==View.GONE){
                items.get(i).callOnClick();
            }
        }
    }
}
