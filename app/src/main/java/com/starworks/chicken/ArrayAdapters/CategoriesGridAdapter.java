package com.starworks.chicken.ArrayAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.starworks.chicken.Constants;
import com.starworks.chicken.ImageHelper;
import com.starworks.chicken.Models.Category;
import com.starworks.chicken.R;

import java.util.List;

/**
 * Created by Lojze on 25.2.2015.
 */
public class CategoriesGridAdapter extends ArrayAdapter<Category> {
    private static final int LAYOUT = R.layout.category_grid_item;

    List<Category> categories;
    Context context;
    int limit;

    public CategoriesGridAdapter(Context context, List<Category> categories){
        super(context, LAYOUT, categories);
        this.context = context;
        this.categories = categories;
        limit = 0;
    }

    public CategoriesGridAdapter setLimit(int limit){
        this.limit = limit;
        return this;
    }

    public int getCatID(int position){
        return categories.get(position).getID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if(limit>0){
            int numCol = parent.getWidth()/100;


        }
        if(view == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            view = inflater.inflate(LAYOUT, parent, false);
        }

        Category category = categories.get(position);

        ImageView imgCatPic = (ImageView)view.findViewById(R.id.imgCatPic);
        // TODO
        //imgCatPic.setImageResource(R.drawable.cat_sport);

        String url = Constants.CAT_IMAGE_BASE_URL + category.getPictureRes();
        ImageLoader.getInstance().displayImage(url, imgCatPic, ImageHelper.getOptions());
        imgCatPic.setColorFilter(getRandomColor());

        TextView lblCategory = (TextView)view.findViewById(R.id.lblCatName);
        lblCategory.setText(category.getName());

        return view;
    }


    int getRandomColor(){
        return Color.parseColor(Constants.colors[(int)(Math.random()*Constants.colors.length)]);

    }
}
