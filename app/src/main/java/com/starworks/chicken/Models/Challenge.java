package com.starworks.chicken.Models;

import com.starworks.chicken.DBHandler;
import com.starworks.chicken.PointsHandler;

import java.sql.Date;
import java.util.List;

public class Challenge {
    private int challengeID;

    private Category category;
    private String name;
    private String description;
    private int points;     // Chicken points
    private int duration;   // Trajanje izziva v minutah
    private int group;
    private int groupOrder;

    private DBHandler db;

    //Konstruktor, ki iz id-ja pridobi podatke
    public Challenge(int chID) {
        db = new DBHandler(null, null, null, 0);

        this.challengeID = chID;
        this.name = db.getName(challengeID);
        this.description = db.getDescription(challengeID);
        this.duration = db.getDuration(challengeID);
        this.points = db.getPoints(challengeID);
        this.category = db.getCategory(challengeID);
        this.group = db.getGroup(challengeID);
        this.groupOrder = db.getGroupOrder(challengeID);
    }

    public Challenge(int id, String name, String description, int points, int duration, Category cat, int group, int groupOrder) {
        db = new DBHandler(null, null, null, 0);

        this.challengeID = id;
        this.name = name;
        this.description = description;
        this.points = points;
        this.duration = duration;
        this.category = cat;
        this.group = group;
        this.groupOrder = groupOrder;
    }

    public int getID() {
        return challengeID;
    }
    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    public int getDuration() {
        return duration;
    }
    public int getPoints() {
        return points;
    }
    public Category getCategory() {
        return category;
    }
    public int getGroup() {
        return group;
    }
    public int getGroupOrder() {
        return groupOrder;
    }
    public String getDurationString() {
        if (getDuration() < 60)
            return getDuration() + "min";
        else if (getDuration() < 3600)
            return (getDuration() / 60) + "h";
        else
            return (getDuration() / 60 / 24) + "d";
    }

    public ActiveChallenge activate() {
        setAccepted();
        return new ActiveChallenge(this, System.currentTimeMillis());
    }

    public static List<Challenge> getAllChallenges() {
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getChallenges(null);
    }

    public static List<Challenge> getAllChallenges(Category category) {
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getChallenges(category);
    }

    public static List<Challenge> getRemainingChallenges() {
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getRemainingChallenges(null);
    }

    public static List<Challenge> getRemainingChallenges(Category category) {
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getRemainingChallenges(category);
    }

    public void setAccepted() {
        db.changeStatus(challengeID, 1);
    }

    public void setDismissed() {
        db.changeStatus(challengeID, 2);
    }

    public void setPostponed() {
        db.changeStatus(challengeID, 3);
    }

    public void setCompleted() {
        db.changeStatus(challengeID, 4);
        User.addCompleted();
        User.addPoints(getPoints());
        PointsHandler.refreshPoints();
    }

    public void setFailed() {
        db.changeStatus(challengeID, 5);
        User.addFailed();
    }

    public void setCanceled() {
        db.changeStatus(challengeID, 0);
        User.addFailed();
    }

    public boolean isGroupChallenge() {
        if (getGroup() == 0)
            return false;
        return true;
    }
}
