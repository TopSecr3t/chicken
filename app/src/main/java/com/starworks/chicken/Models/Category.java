package com.starworks.chicken.Models;

import com.starworks.chicken.DBHandler;
import com.starworks.chicken.DataStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lojze on 20.2.2015.
 */
public class Category {

    private int categoryID;
    private String name;
    private String pictureRes;

    public Category (int id, String name, String pictureRes){
        this.name = name;
        this.pictureRes = pictureRes;
        this.categoryID = id;
    }

    public Category (int id) {
        DBHandler db = new DBHandler(null, null, null, 0);

        this.categoryID = id;
        this.name = db.getCategName(id);
        this.pictureRes = db.getCategPictureURI(id);
    }

    public String getName(){
        return name;
    }

    public String getPictureRes(){
        return pictureRes;
    }

    public int getID() {
        return categoryID;
    }

    public static List<Category> getAllCategories() {
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getCategories();
    }

    public List<Challenge> getChallenges(){
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getChallenges(this);
    }

    public List<Challenge> getRemainingChallenges(){
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getRemainingChallenges(this);
    }
}
