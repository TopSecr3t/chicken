package com.starworks.chicken.Models;

import android.util.Log;

import com.starworks.chicken.DBHandler;

import java.util.List;

/**
 * Created by Lojze on 18.2.2015.
 */
public class ActiveChallenge extends Challenge {
    private long startDate;
    private long endDate;

    public ActiveChallenge(Challenge c, long startDate){
        super(c.getID(), c.getName(), c.getDescription(), c.getPoints(), c.getDuration(), c.getCategory(), c.getGroup(), c.getGroupOrder());

        this.startDate = startDate;
        this.endDate = startDate + c.getDuration() * 60 * 1000;
    }

    //Izracuna koliksen del casa je ze potekel
    public double getProgress() {
        return (1 - (float)(endDate - System.currentTimeMillis()) / (getDuration() * 60 * 1000));
    }

    public String getTimeLeft() {
        double timeLeft = (endDate - System.currentTimeMillis()) / (60 * 1000);

        if (timeLeft < 60)
            return Math.round(timeLeft) + "min";
        else if (timeLeft < 3600)
            return Math.round((timeLeft / 60)) + "h";
        else
            return Math.round((timeLeft / 60 / 24)) + "d";
    }

    public static List<ActiveChallenge> getActiveChallenges() {
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getActiveChallenges(null);
    }

    public static List<ActiveChallenge> getActiveChallenges(Category category) {
        DBHandler db = new DBHandler(null, null, null, 0);
        return db.getActiveChallenges(category);
    }
}
