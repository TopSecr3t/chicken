package com.starworks.chicken.Models;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Aljaz on 26/03/15.
 */
public class User {

    private static Context cont;

    public static void setContext(Context context) {
        cont = context;
    }

    public static int getPoints() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        return sharedPref.getInt("points", 0);
    }

    public static String getImage() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        return sharedPref.getString("image", "");
    }

    public static void addPoints(int points) {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("points", getPoints() + points);
        editor.apply();
    }

    public static void setPoints(int points) {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("points", points);
        editor.apply();
    }

    public static String getName() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        return sharedPref.getString("name", "User");
    }

    public static int getCompletedNum() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        return sharedPref.getInt("completedNum", 0);
    }

    public static void addCompleted() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("completedNum", sharedPref.getInt("completedNum", 0) + 1);
        editor.apply();
    }

    public static int getFailedNum() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        return sharedPref.getInt("failedNum", 0);
    }

    public static void addFailed() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("failedNum", sharedPref.getInt("failedNum", 0) + 1);
        editor.apply();
    }

    public static boolean isSignedIn() {
        SharedPreferences sharedPref = cont.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        if (sharedPref.getString("id", "0").equals("0"))
            return false;
        return true;
    }
}
