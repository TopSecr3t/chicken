package com.starworks.chicken.Models;

import com.starworks.chicken.DBHandler;
import com.starworks.chicken.PointsHandler;

import java.util.List;

/**
 * Created by Aljaz on 16/03/15.
 */
public class Group extends Challenge {

    private Challenge currentChallenge;
    private int id;
    private List<Challenge> challenges;

    public Group(int chID) {
        super(chID);

        DBHandler db = new DBHandler(null, null, null, 0);

        this.id = super.getGroup();
        this.challenges = db.getGroupChallenges(this);

        this.currentChallenge = challenges.get(1);
    }

    public int getId() {
        return id;
    }

    @Override
    public ActiveChallenge activate() {
        super.activate();

        return new ActiveChallenge(challenges.get(1), System.currentTimeMillis());
    }

    @Override
    public void setCompleted() {
        super.setCompleted();

        if (currentChallenge.getGroupOrder() < challenges.size()) {
            currentChallenge = challenges.get(currentChallenge.getGroupOrder() + 1);
        } else {
            currentChallenge = null;
            User.addPoints(challenges.get(0).getPoints());
        }
    }

    public Challenge getChallenge() {
        return currentChallenge;
    }

    public boolean hasRemainingChallenges() {
        if (currentChallenge == null)
            return false;
        return true;
    }
}
