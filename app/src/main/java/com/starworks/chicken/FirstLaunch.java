package com.starworks.chicken;

import android.content.Context;
import android.content.SharedPreferences;

import com.starworks.chicken.Models.Category;
import com.starworks.chicken.Models.Challenge;

/**
 * Created by Aljaz on 24/02/15.
 */
public class FirstLaunch {
    private static Context context;

    public static void setContext(Context c) {
        context = c;
    }

    public static void check() {
        SharedPreferences sharedPref = context.getSharedPreferences("com.starworks.chicken.preferences", Context.MODE_PRIVATE);

        if (sharedPref.getBoolean("first_launch", true)) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean("first_launch", false);
            editor.apply();

            setDefaultDB();
        }
    }

    public static void setDefaultDB() {
        Category[] categories = {
                new Category(1, "Sport", ""),
                new Category(2, "Social", ""),
                new Category(3, "Drawing", ""),
                new Category(4, "Dare Challenges", ""),
                new Category(5, "Other", "")
        };

        Challenge[] challenges = {
                new Challenge(1, "Daily Workout Challenge", "Do 15 pushups", 1000, 60*24, categories[0], 0, 0),
                new Challenge(2, "Old Friends", "Call a friend you haven t spoken to in ages", 1000, 60*24, categories[1], 0, 0),
                new Challenge(3, "Go for a run", "Run for at least 10 min", 1000, 60*24, categories[0], 0, 0),
                new Challenge(5, "Learn a new language", "Learn 20 words in a foreign language of your desire.",1000, 7*24*60, categories[4], 0, 0),
        };

        DBHandler db = new DBHandler(null, null, null, 0);
        db.updateDB(categories);
        db.updateDB(challenges);
    }
}
