package com.starworks.chicken.Listeners;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.starworks.chicken.Fragments.HomeFragment;
import com.starworks.chicken.Models.Challenge;
import com.starworks.chicken.R;

/**
 * Created by Lojze on 15.3.2015.
 */
public class ActiveChallengeOptionOnClickListener implements View.OnClickListener {
    public static final int OPTION_CANCEL = 0;
    public static final int OPTION_STOP = 2;
    public static final int OPTION_COMPLETE = 3;
    public static final int OPTION_DETAILS = 1;

    Challenge challenge;
    View item;

    public ActiveChallengeOptionOnClickListener(Challenge challenge, View item){
        this.challenge = challenge;
        this.item = item;
    }

    @Override
    public void onClick(View v) {
        int option = ((ViewGroup)v.getParent()).indexOfChild(v);

        switch (option){
            case OPTION_CANCEL:
                item.callOnClick();
                break;
            case OPTION_STOP:
                challenge.setCanceled();
                HomeFragment.fragment.updateLists();
                break;
            case OPTION_COMPLETE:
                challenge.setCompleted();
                HomeFragment.fragment.updateLists();
                break;
            case OPTION_DETAILS:
                //TODO: Create an activity to display info about the challenge
                Toast.makeText(v.getContext(), challenge.getDescription(), Toast.LENGTH_LONG).show();
                break;
        }
    }
}
