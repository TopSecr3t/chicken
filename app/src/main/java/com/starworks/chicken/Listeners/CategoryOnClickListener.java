package com.starworks.chicken.Listeners;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.starworks.chicken.ArrayAdapters.*;
import com.starworks.chicken.MainActivity;
import com.starworks.chicken.ViewCategoryActivity;

/**
 * Created by Lojze on 25.2.2015.
 */
public class CategoryOnClickListener implements AdapterView.OnItemClickListener{
    private Context context;
    public CategoryOnClickListener(Context context){
        this.context = context;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(context, ViewCategoryActivity.class);
        int catID = ((CategoriesGridAdapter)parent.getAdapter()).getCatID(position);
        intent.putExtra(MainActivity.CATEGORY_ID, catID);
        context.startActivity(intent);
    }
}
