package com.starworks.chicken.Listeners;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.starworks.chicken.DataStore;
import com.starworks.chicken.Models.Challenge;
import com.starworks.chicken.R;

/**
 * Created by Lojze on 15.3.2015.
 */
public class ChallengeOnClickListener implements View.OnClickListener {
    boolean isHidden;
    public ChallengeOnClickListener(){
        isHidden = false;
    }

    @Override
    public void onClick(View v) {
        final View info = v.findViewById(R.id.viewChallengeOverlay);

        isHidden = info.getVisibility() == View.GONE;

        Animation animation;
        if(isHidden) {
            animation = AnimationUtils.loadAnimation(info.getContext(), R.anim.challenge_slide_in);
            isHidden = false;
        }
        else{
            animation = AnimationUtils.loadAnimation(info.getContext(), R.anim.challenge_slide_out);
            isHidden = true;
        }

        animation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
               if(isHidden){
                   info.setVisibility(View.GONE);
               }
                else{
                   info.setVisibility(View.VISIBLE);
               }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        info.startAnimation(animation);
    }
}
