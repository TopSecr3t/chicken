package com.starworks.chicken.Listeners;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.starworks.chicken.Fragments.HomeFragment;
import com.starworks.chicken.Models.Challenge;

/**
 * Created by Lojze on 15.3.2015.
 */
public class SuggestedChallengeOptionOnClickListener implements View.OnClickListener {
    public static final int OPTION_CANCEL = 0;
    public static final int OPTION_DETAILS = 1;
    public static final int OPTION_ACCEPT = 2;

    Challenge challenge;
    View item;

    public SuggestedChallengeOptionOnClickListener(Challenge challenge, View item){
        this.challenge = challenge;
        this.item = item;
    }

    @Override
    public void onClick(View v) {
        int option = ((ViewGroup)v.getParent()).indexOfChild(v);

        switch (option){
            case OPTION_CANCEL:
                item.callOnClick();
                break;
            case OPTION_ACCEPT:
                challenge.activate();
                HomeFragment.fragment.updateLists();
                break;
            case OPTION_DETAILS:
                //TODO: Create an activity to display info about the challenge
                Toast.makeText(v.getContext(), challenge.getDescription(), Toast.LENGTH_LONG).show();
                break;
        }


    }
}
