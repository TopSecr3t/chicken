package com.starworks.chicken;

import android.content.Context;
import android.util.Log;

import com.starworks.chicken.Models.Category;
import com.starworks.chicken.Models.Challenge;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class OnlineDB {
    private static final String CHALL_DB_LINK = "http://ayangames.com/chicken/izpisi_challenges_db.php";
    private static final String CATEG_DB_LINK = "http://ayangames.com/chicken/izpisi_categories_db.php";

    private static Context cont;

    public static void setContext(Context context) {
        cont = context;
    }

    // prebere mySQL bazo in jo posreduje 'Database' skripti
    public static void refreshDB() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                InputStream inputStreamChall = null;
                InputStream inputStreamCateg = null;
                String JSONChall = "";
                String JSONCateg = "";
                Challenge[] challenges = null;
                Category[] categories = null;

                // vspostavi povezavo z php skripto
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPostChall = new HttpPost(CHALL_DB_LINK);
                    HttpResponse httpResponseChall = httpClient.execute(httpPostChall);
                    HttpEntity httpEntityChall = httpResponseChall.getEntity();
                    inputStreamChall = httpEntityChall.getContent();

                    HttpPost httpPostCateg = new HttpPost(CATEG_DB_LINK);
                    HttpResponse httpResponseCateg = httpClient.execute(httpPostCateg);
                    HttpEntity httpEntityCateg = httpResponseCateg.getEntity();
                    inputStreamCateg = httpEntityCateg.getContent();
                } catch (Exception e) {
                    Log.e("log_tag", "Napaka v http povezavi: " + e.toString());
                    return;
                }
                // shrani bazo v jSon obliki
                try {
                    BufferedReader readerChall = new BufferedReader(new InputStreamReader(inputStreamChall, "iso-8859-1"), 8);
                    StringBuilder builderChall = new StringBuilder();
                    String vrstaChall = readerChall.readLine();

                    while (vrstaChall != null) {
                        builderChall.append(vrstaChall + "\n");
                        vrstaChall = readerChall.readLine();
                    }
                    inputStreamChall.close();
                    JSONChall = builderChall.toString();

                    BufferedReader readerCateg = new BufferedReader(new InputStreamReader(inputStreamCateg, "iso-8859-1"), 8);
                    StringBuilder builderCateg = new StringBuilder();
                    String vrstaCateg = readerCateg.readLine();

                    while (vrstaCateg != null) {
                        builderCateg.append(vrstaCateg + "\n");
                        vrstaCateg = readerCateg.readLine();
                    }
                    inputStreamCateg.close();
                    JSONCateg = builderCateg.toString();
                } catch (Exception e) {
                    Log.e("log_tag", "Napaka pri prevajanju rezultata: " + e.toString());
                    return;
                }
                try {
                    JSONArray jArrayChall = new JSONArray(JSONChall);

                    challenges = new Challenge[jArrayChall.length()];

                    for (int n = 0; n < jArrayChall.length(); n++) {
                        JSONObject jObjectChall = jArrayChall.getJSONObject(n);

                        challenges[n] = new Challenge(
                                Integer.parseInt(jObjectChall.getString("ID")),
                                jObjectChall.getString("Name"),
                                jObjectChall.getString("Description"),
                                Integer.parseInt(jObjectChall.getString("Points")),
                                Integer.parseInt(jObjectChall.getString("Duration")),
                                // TODO
                                new Category(Integer.parseInt(jObjectChall.getString("Category")), "", ""),
                                Integer.parseInt(jObjectChall.getString("Group")),
                                Integer.parseInt(jObjectChall.getString("GroupOrder"))
                        );
                    }
                } catch (Exception e) {
                    Log.e("log_tag", "Napaka pri vpisu podatkov v String: " + e.toString());
                    return;
                }
                try {
                    JSONArray jArrayCateg = new JSONArray(JSONCateg);

                    categories = new Category[jArrayCateg.length()];

                    for (int n = 0; n < jArrayCateg.length(); n++) {
                        JSONObject jObjectCateg = jArrayCateg.getJSONObject(n);

                        categories[n] = new Category(
                                Integer.parseInt(jObjectCateg.getString("ID")),
                                jObjectCateg.getString("Name"),
                                jObjectCateg.getString("PictureURI")
                        );
                    }
                } catch (Exception e) {
                    Log.e("log_tag", "Napaka pri vpisu podatkov v String: " + e.toString());
                    return;
                }

                DBHandler db = new DBHandler(null, null, null, 0);
                db.updateDB(categories);
                db.updateDB(challenges);
            }
        };
        Thread thread = new Thread(r);
        thread.start();
    }
}
