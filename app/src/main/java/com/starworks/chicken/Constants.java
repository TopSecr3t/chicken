package com.starworks.chicken;

/**
 * Created by Lojze on 18.2.2015.
 */
public class Constants {
    public static final String ARG_SECTION_NUMBER = "section_number";

    public static final int SUGGESTED_CHALLENGES_COUNT = 3;

    public static final String colors[] = {"#C54854", "#ca492c", "#4874c5", "#8ad14b", "#ddd556", "#509cb5"};

    public static final String CAT_IMAGE_BASE_URL = "http://www.ayangames.com/chicken/images/categories/";
}
